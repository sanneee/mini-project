import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
// import { HomeService } from '../../features/home/services/home.service';
import { Manga } from '../models/manga.model';
import { environment } from '../../../environments/environment';
import { empty, Subject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class MangaService {
  public apiData = new Subject<any>();
  private categories: string[] = [];

  constructor(private http: HttpClient) {}

  fetchManga() {
    return this.http.get<Manga[]>(`${environment.firebaseRestAPI}/manga.json`);
  }

  fetchMangaDetail(mangaId: number) {
    return this.http.get<Manga>(
      `${environment.firebaseRestAPI}/manga/${mangaId}.json`
    );
  }

  fetchCategories() {
    return this.fetchManga().pipe(
      map((resData) => {
        return resData.map((ele) => {
          return ele['categories'];
        });
      }),
      tap((resData) => {
        this.categories = [...new Set(resData.flat(1))];
      })
    );
  }

  getCategories() {
    return this.categories;
  }

  //Fav
  writeFavorite(
    mangaFavorite: number,
    addStatus: boolean,
    allMangaFavorite: number[]
  ) {
    const userData: {
      displayName: string;
      email: string;
      id: string;
      _token: string;
      _tokenExpirationDate: string;
    } = JSON.parse(localStorage.getItem('userData')!);

    if (!userData) {
      return empty();
    }

    if (addStatus) {
      if (allMangaFavorite == null) {
        allMangaFavorite = [mangaFavorite];
      }
      allMangaFavorite.push(mangaFavorite);
    } else {
      allMangaFavorite = allMangaFavorite.filter(function (item) {
        return item !== mangaFavorite;
      });
    }

    let currentMangaFavorite = [...new Set(allMangaFavorite)];

    return this.http.put<number[]>(
      `${environment.firebaseRestAPI}/users/${userData.id}/favorite.json`,
      { favorite: currentMangaFavorite }
    );
  }

  fetchFavorite() {
    const userData: {
      displayName: string;
      email: string;
      id: string;
      _token: string;
      _tokenExpirationDate: string;
    } = JSON.parse(localStorage.getItem('userData')!);

    if (!userData) {
      return empty();
    }

    return this.http
      .get<number[]>(
        `${environment.firebaseRestAPI}/users/${userData.id}/favorite/favorite.json`
      )
      .pipe(tap((resData) => this.apiData.next(resData)));
  }

  //History
  writeHistory(mangaHistory: number, allMangaHistory: number[]) {
    const userData: {
      displayName: string;
      email: string;
      id: string;
      _token: string;
      _tokenExpirationDate: string;
    } = JSON.parse(localStorage.getItem('userData')!);

    if (!userData) {
      return empty();
    }

    if (allMangaHistory == null) {
      allMangaHistory = [mangaHistory];
    }
    allMangaHistory.push(mangaHistory);
    let currentMangaHistory = [...new Set(allMangaHistory)];

    return this.http.put<number[]>(
      `${environment.firebaseRestAPI}/users/${userData.id}/history.json`,
      { history: currentMangaHistory.filter((e) => e != null) }
    );
  }

  fetchHistory() {
    const userData: {
      displayName: string;
      email: string;
      id: string;
      _token: string;
      _tokenExpirationDate: string;
    } = JSON.parse(localStorage.getItem('userData')!);

    if (!userData) {
      return empty();
    }

    return this.http.get<number[]>(
      `${environment.firebaseRestAPI}/users/${userData.id}/history/history.json`
    );
  }
}
