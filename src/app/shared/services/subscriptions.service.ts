import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, empty, filter, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';

export interface Subscription {
  title: string;
  period: number;
  style: string;
  dateEnd: Date;
}

@Injectable({
  providedIn: 'root',
})
export class SubscriptionsService {
  subscript = new BehaviorSubject<Subscription>(null!);

  constructor(private http: HttpClient) {}
  //

  private networkStatus = new Subject<boolean>();

  get isOnline$() {
    return this.networkStatus.pipe(filter(Boolean));
  }
  //

  addSubscription(plan: Subscription) {
    const userData: {
      displayName: string;
      email: string;
      id: string;
      _token: string;
      _tokenExpirationDate: string;
    } = JSON.parse(localStorage.getItem('userData')!);

    if (!userData) {
      return empty();
    }

    return this.http.put<number[]>(
      `${environment.firebaseRestAPI}/users/${userData.id}/subscription.json`,
      plan
    );
  }

  getSubscription() {
    const userData: {
      displayName: string;
      email: string;
      id: string;
      _token: string;
      _tokenExpirationDate: string;
    } = JSON.parse(localStorage.getItem('userData')!);
    if (!userData) {
      return empty();
    }
    return this.http.get<Subscription>(
      `${environment.firebaseRestAPI}/users/${userData.id}/subscription.json`
    );
  }
}
