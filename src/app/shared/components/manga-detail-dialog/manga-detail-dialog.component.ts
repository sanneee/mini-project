import { Component, Inject, OnInit } from '@angular/core';

import {
  trigger,
  state,
  style,
  transition,
  animate,
} from '@angular/animations';
import { Router } from '@angular/router';
import { Manga } from 'src/app/shared/models/manga.model';

import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { AuthService } from '../../../features/auth/services/auth.service';
import { MangaService } from '../../services/manga.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-manga-detail-dialog',
  templateUrl: './manga-detail-dialog.component.html',
  styleUrls: ['./manga-detail-dialog.component.scss'],
  animations: [
    trigger('sortby', [
      state('asc', style({ transform: 'rotate(0)' })),
      state('desc', style({ transform: 'rotate(-180deg)' })),
      transition('asc <=> desc', animate('200ms ease-out')),
    ]),
  ],
})
export class MangaDetailDialogComponent implements OnInit {
  displayName!: String;
  isLoading = false;
  mangaDetail: Manga;
  favorite: boolean = false;
  allFavorite: number[] = [];

  sortByState = 'asc';

  isAuthenticated = false;
  userSub!: Subscription;

  constructor(
    public dialogRef: MatDialogRef<MangaDetailDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private authService: AuthService,
    private router: Router,
    private mangaService: MangaService
  ) {
    //get Data to this Dialog
    this.mangaDetail = data['manga'];
  }

  ngOnInit(): void {
    this.userSub = this.authService.user.subscribe((user) => {
      this.isAuthenticated = !!user;
      //Check Auth
      if (this.isAuthenticated) {
        this.isLoading = true;
        //Load Favorite Manga
        this.mangaService.fetchFavorite().subscribe((resData) => {
          this.favorite = (resData ? resData : []).some(
            (ele) => ele == this.mangaDetail.id
          );
          this.allFavorite = resData;
          this.mangaService.apiData.next(resData);
          this.isLoading = false;
        });
      }
    });
  }

  //Go to Content Page
  toContent(chapterId: number) {
    this.onClose();
    this.router.navigate([
      'contents',
      this.mangaDetail.name,
      this.mangaDetail.id,
      chapterId,
    ]);
  }

  sortBy() {
    this.sortByState = this.sortByState == 'desc' ? 'acs' : 'desc';
    this.mangaDetail.chapters.reverse();
  }

  onToggleFav() {
    this.isLoading = true;
    this.mangaService
      .writeFavorite(this.mangaDetail.id, !this.favorite, this.allFavorite)
      .subscribe((resData) => {
        this.mangaService.fetchFavorite().subscribe((resData) => {
          this.favorite = (resData ? resData : []).some(
            (ele) => ele == this.mangaDetail.id
          );
          this.isLoading = false;
          this.allFavorite = resData;
        });
      });
  }

  //Close This Dialog
  onClose(): void {
    this.dialogRef.close();
  }
}
