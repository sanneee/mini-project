import { style } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscript } from '../../models/subscript.model';
import { SubscriptionsService } from '../../services/subscriptions.service';
import { DisplayNameDialogComponent } from '../display-name-dialog/display-name-dialog.component';

@Component({
  selector: 'app-subscription-dialog',
  templateUrl: './subscription-dialog.component.html',
  styleUrls: ['./subscription-dialog.component.scss'],
})
export class SubscriptionDialogComponent implements OnInit {
  plans!: Subscript[];
  isLoading: boolean = false;

  selectPlan!: {
    title: string;
    period: number;
    style: string;
  };

  constructor(
    public dialogRef: MatDialogRef<DisplayNameDialogComponent>,
    private subscriptionService: SubscriptionsService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.plans = [
      {
        title: 'Normal',
        period: 1,
        style: 'bg-gray-100 text-gray-800',
      },
      {
        title: 'Premium',
        period: 5,
        style: 'bg-blue-100 text-blue-800 ',
      },
      {
        title: 'VVIP',
        period: 10,
        style: 'bg-yellow-100 text-yellow-800',
      },
    ];

    this.dialogRef.afterClosed().subscribe((result) => {
      this.router.navigate([], { relativeTo: this.route });
    });
  }

  onClick(plan: { title: string; period: number; style: string }) {
    this.selectPlan = plan;
  }

  addSubscription() {
    this.isLoading = true;
    this.subscriptionService
      .addSubscription({
        ...this.selectPlan,
        ...{
          dateEnd: new Date(
            new Date().getTime() + this.selectPlan.period * 60000
          ),
        },
      })
      .subscribe((res) => {
        this.subscriptionService.getSubscription().subscribe((res) => {
          this.subscriptionService.subscript.next(res);
          this.isLoading = false;
          this.onClose();
        });
      });
  }

  onClose(): void {
    this.dialogRef.close();
  }
}
