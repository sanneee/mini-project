import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { AuthService } from '../../../features/auth/services/auth.service';
import { DisplayNameDialogComponent } from '../display-name-dialog/display-name-dialog.component';
import { Router } from '@angular/router';
import { SubscriptionDialogComponent } from '../subscription-dialog/subscription-dialog.component';
import { SubscriptionsService } from '../../services/subscriptions.service';
import { Subscript } from '../../models/subscript.model';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class NavbarComponent implements OnInit {
  isAuthenticated = false;

  userSub!: Subscription;
  subscript!: Subscription;

  plan!: Subscript;
  displayName: String = '';
  email: String = '';

  constructor(
    public authService: AuthService,
    public dialog: MatDialog,
    public router: Router,
    private subscriptionService: SubscriptionsService
  ) {}

  //Dialog
  openUpdateDisplayName() {
    this.dialog.open(DisplayNameDialogComponent, {
      maxWidth: '90vw',
      width: '400px',
      data: { displayName: this.displayName },
    });
  }

  openSubsciption() {
    this.dialog.open(SubscriptionDialogComponent, {
      width: '500px',
    });
  }

  //ngOnInit
  ngOnInit(): void {
    this.userSub = this.authService.user.subscribe((user) => {
      this.isAuthenticated = !!user;
      this.displayName = '';
      this.email = '';

      if (this.isAuthenticated) {
        this.displayName = user.displayName;
        this.email = user.email;

        this.subscript = this.subscriptionService
          .getSubscription()
          .subscribe((res) => {
            if (res) {
              this.subscriptionService.subscript.next(res);
              if (new Date(res.dateEnd) > new Date()) this.plan = res;
            }
          });

        this.subscriptionService.subscript.subscribe((res) => {
          if (res) {
            if (new Date(res.dateEnd) > new Date()) this.plan = res;
          }
        });
      }
    });
  }

  logout() {
    this.authService.logout;
  }
}
