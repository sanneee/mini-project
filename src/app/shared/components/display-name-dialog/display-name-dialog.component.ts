import { HttpClient } from '@angular/common/http';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';

import { environment } from '../../../../environments/environment';

import {
  MatDialog,
  MAT_DIALOG_DATA,
  MatDialogRef,
} from '@angular/material/dialog';
import {
  AuthResponseData,
  AuthService,
} from '../../../features/auth/services/auth.service';

@Component({
  selector: 'app-display-name-dialog',
  templateUrl: './display-name-dialog.component.html',
  styleUrls: ['./display-name-dialog.component.scss'],
})
export class DisplayNameDialogComponent implements OnInit {
  displayNameForm!: FormGroup;

  displayName!: String;
  isLoading = false;

  constructor(
    public dialogRef: MatDialogRef<DisplayNameDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private authService: AuthService,
    private http: HttpClient
  ) {
    this.displayNameForm = new FormGroup({
      displayName: new FormControl(null, [
        Validators.required,
        Validators.minLength(4),
      ]),
    });

    this.displayNameForm.setValue({ displayName: data['displayName'] });
  }

  ngOnInit(): void {}

  onUpdate() {
    const userData: {
      displayName: string;
      email: string;
      id: string;
      _token: string;
      _tokenExpirationDate: string;
    } = JSON.parse(localStorage.getItem('userData')!);

    this.isLoading = true;

    return this.http
      .post<AuthResponseData>(
        'https://identitytoolkit.googleapis.com/v1/accounts:update?key=' +
          environment.firebaseAPIKey,
        {
          idToken: userData._token,
          displayName: this.displayNameForm.value['displayName'],
          returnSecureToken: true,
        }
      )
      .subscribe(
        (resData) => {
          this.authService.handleAuthentication(
            resData.displayName,
            resData.email,
            resData.localId,
            userData._token,
            parseInt(userData._tokenExpirationDate)
          );

          this.isLoading = false;
          this.onClose();
        },
        (err) => {
          this.isLoading = false;
          return this.authService.handleError(err);
        }
      );
  }

  onClose(): void {
    this.dialogRef.close();
  }
}
