import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterCategory',
})
export class FilterCategoryPipe implements PipeTransform {
  transform(value: any, filterCategory: string): any {

    if (filterCategory === 'All Category') {
      return value;
    }

    const filterValue = [];

    for (let item of value) {
      for (let category of item['categories']) {
        if (category === filterCategory) {
          filterValue.push(item);
          break;
        }
      }
    }

    return filterValue;
  }
}
