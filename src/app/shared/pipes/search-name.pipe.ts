import { LowerCasePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchName',
})
export class SearchNamePipe implements PipeTransform {
  transform(value: any, search: string): any {
    if (search === '') {
      return value;
    }

    const searchedValue = [];

    for (let item of value) {
      if (
        item['name'].toLocaleLowerCase().includes(search.toLocaleLowerCase())
      ) {
        searchedValue.push(item);
      }
    }
    return searchedValue;
  }
}
