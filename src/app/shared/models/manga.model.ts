

export class Manga {
  public id: number;
  public name: string;
  public coverImage: string;
  public desc: string;
  public author: string;
  public categories: string[];
  public chapters: Array<any>;

  constructor( id: number ,name: string, coverImage: string, desc: string, author: string ,categories: string[], chapters: Array<any>) {
    this.id = id;
    this.name = name;
    this.coverImage = coverImage;
    this.desc = desc;
    this.author = author
    this.categories = categories;
    this.chapters = chapters;
  }
}
