import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './components/navbar/navbar.component';
import { DisplayNameDialogComponent } from './components/display-name-dialog/display-name-dialog.component';
import { MatMenuModule } from '@angular/material/menu';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { RouterModule } from '@angular/router';
import { SearchNamePipe } from './pipes/search-name.pipe';
import { FilterCategoryPipe } from './pipes/filter-category.pipe';
import { MangaDetailDialogComponent } from './components/manga-detail-dialog/manga-detail-dialog.component';
import { SubscriptionDialogComponent } from './components/subscription-dialog/subscription-dialog.component';
import { HttpInterceptorService } from './services/http-interceptor.service';

@NgModule({
  declarations: [
    NavbarComponent,
    DisplayNameDialogComponent,
    SearchNamePipe,
    FilterCategoryPipe,
    MangaDetailDialogComponent,
    SubscriptionDialogComponent,
  ],
  imports: [
    CommonModule,
    MatMenuModule,
    MatDialogModule,
    ReactiveFormsModule,
    RouterModule,
  ],
  exports: [
    NavbarComponent,
    DisplayNameDialogComponent,
    MangaDetailDialogComponent,
    SearchNamePipe,
    FilterCategoryPipe,
  ],
})
export class SharedModule {}
