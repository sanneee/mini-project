import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth/auth.guard';
import { MymangaComponent } from './pages/mymanga/mymanga.component';

const routes: Routes = [
  {
    path: '',
    component: MymangaComponent,
    canActivate: [AuthGuard],
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MymangaRoutingModule {}
