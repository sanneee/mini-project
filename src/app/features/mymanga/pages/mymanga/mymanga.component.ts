import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { SubscriptionDialogComponent } from 'src/app/shared/components/subscription-dialog/subscription-dialog.component';

@Component({
  selector: 'app-mymanga',
  templateUrl: './mymanga.component.html',
  styleUrls: ['./mymanga.component.scss'],
})
export class MymangaComponent implements OnInit {
  constructor(private route: ActivatedRoute, public dialog: MatDialog) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      if (params['subscription']) {
        this.openSubsciption();
      }
    });
  }

  openSubsciption() {
    this.dialog.open(SubscriptionDialogComponent, {
      width: '500px',
    });
  }
}
