import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MangaDetailDialogComponent } from 'src/app/shared/components/manga-detail-dialog/manga-detail-dialog.component';
import { Manga } from 'src/app/shared/models/manga.model';
import { MymangaService } from '../../services/mymanga.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss'],
})
export class HistoryComponent implements OnInit {
  mangaLists: any[] = [];
  categories!: any[];
  isLoading = true;

  constructor(
    private dialog: MatDialog,
    private mymangaService: MymangaService
  ) {}

  ngOnInit(): void {
    this.fetchHistory();
  }

  fetchHistory() {
    this.mymangaService.fetchMangaHistory().subscribe((resData) => {
      this.mangaLists = resData;
      this.isLoading = false;
    });
  }

  openDialog(manga: Manga) {
    this.dialog.open(MangaDetailDialogComponent, {
      width: '600px',
      maxWidth: '90vw',
      data: { manga: manga },
    });
  }
}
