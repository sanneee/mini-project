import { Component, OnDestroy, OnInit } from '@angular/core';
import { Manga } from 'src/app/shared/models/manga.model';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MangaService } from 'src/app/shared/services/manga.service';
import { MymangaService } from '../../services/mymanga.service';
import { MangaDetailDialogComponent } from 'src/app/shared/components/manga-detail-dialog/manga-detail-dialog.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-favorite',
  templateUrl: './favorite.component.html',
  styleUrls: ['./favorite.component.scss'],
})
export class FavoriteComponent implements OnInit {

  mangaLists: any[] = [];
  isLoading = true;
  //Search
  search: string = '';
  selectedCategory = 'All Category';
  categories!: any[];

  categoriesSub = Subscription;

  constructor(
    private dialog: MatDialog,
    private mangaService: MangaService,
    private mymangaService: MymangaService
  ) {
    this.getCategories();
    this.fetchFavorite()
    this.mangaService.fetchFavorite().subscribe((resData) =>
      this.mymangaService.fetchMangaFavorite(resData).subscribe((resData) => {


        this.mangaLists = resData;
      })

    );
  }

  ngOnInit(): void {}


  fetchFavorite() {
    this.mangaService.apiData.subscribe((data) => {
      this.mymangaService.fetchMangaFavorite(data).subscribe((resData) => {
        this.isLoading = false;
        this.mangaLists = resData;
      });
    });
  }

  getCategories() {
    this.mangaService.fetchCategories().subscribe(() => {
      this.categories = this.mangaService.getCategories();
      this.categories.unshift('All Category');
    });
  }

  onSelect(selectedCategory: string) {
    this.selectedCategory = selectedCategory;
  }

  openDialog(manga: Manga) {
    this.dialog.open(MangaDetailDialogComponent, {
      width: '600px',
      maxWidth: '90vw',
      data: { manga: manga },
    });
  }
}
