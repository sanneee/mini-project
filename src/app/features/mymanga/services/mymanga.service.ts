import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import { MangaService } from '../../../../app/shared/services/manga.service';

@Injectable({
  providedIn: 'root',
})
export class MymangaService {
  constructor(private mangaService: MangaService) {}

  fetchMangaFavorite(favoriteId: number[]) {
    return this.mangaService.fetchManga().pipe(
      map((resData) => {
        return resData
          .map((ele) => {
            if (favoriteId) {
              if (favoriteId.some((e) => ele['id'] === e)) {
                return ele;
              }
            }
            return null;
          })
          .filter((ele) => ele != null);
      })
    );
  }

  fetchMangaHistory() {
    let historyId: number[];
    this.mangaService
      .fetchHistory()
      .subscribe((resData) => (historyId = resData ? resData : []));

    return this.mangaService.fetchManga().pipe(
      map((resData) => {
        return resData
          .map((ele) => {
            if (historyId) {
              if (historyId.some((e) => ele['id'] == e)) {
                return ele;
              }
            }
            return null;
          })
          .filter((ele) => ele != null);
      })
    );
  }
}
