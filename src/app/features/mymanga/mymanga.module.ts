import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MymangaComponent } from './pages/mymanga/mymanga.component';
import { RouterModule } from '@angular/router';
import { MymangaRoutingModule } from './mymanga-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { HistoryComponent } from './components/history/history.component';
import { FavoriteComponent } from './components/favorite/favorite.component';
import { FormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MatMenuModule } from '@angular/material/menu';

@NgModule({
  declarations: [MymangaComponent, HistoryComponent, FavoriteComponent],
  imports: [
    CommonModule,
    SharedModule,
    MymangaRoutingModule,
    FormsModule,
    MatSelectModule,
    MatMenuModule,
  ],
})
export class MymangaModule {}
