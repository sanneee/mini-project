import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ContentsComponent } from './pages/contents/contents.component';
import { ContentsGuard } from './contents.guard';

@NgModule({
  declarations: [ContentsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: ContentsComponent,
        canActivate: [ContentsGuard],
      },
    ]),
  ],
})
export class ContentsModule {}
