import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
  UrlTree,
  ActivatedRoute,
} from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { SubscriptionsService } from 'src/app/shared/services/subscriptions.service';

@Injectable({ providedIn: 'root' })
export class ContentsGuard implements CanActivate {
  isAuth!: boolean;
  constructor(
    private subscriptionsService: SubscriptionsService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    router: RouterStateSnapshot
  ):
    | boolean
    | UrlTree
    | Promise<boolean | UrlTree>
    | Observable<boolean | UrlTree> {
    return JSON.parse(localStorage.getItem('userData')!)
      ? this.subscriptionsService.getSubscription().pipe(
          map((plan) => {
            if (plan) {
              if (new Date(plan.dateEnd) > new Date()) {
                return true;
              }

              return !this.router.url.includes('contents')
                ? this.router.createUrlTree([], {
                    queryParams: { subscription: true },
                    relativeTo: this.route,
                  })
                : this.router.createUrlTree(['/']);
            }
            return this.router.createUrlTree([], {
              queryParams: { subscription: true },
              relativeTo: this.route,
            });
          })
        )
      : this.router.createUrlTree(['/auth']);
  }
}
