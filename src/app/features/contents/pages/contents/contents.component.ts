import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';

import { MangaDetailDialogComponent } from '../../../../shared/components/manga-detail-dialog/manga-detail-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { MangaService } from 'src/app/shared/services/manga.service';

@Component({
  selector: 'app-contents',
  templateUrl: './contents.component.html',
  styleUrls: ['./contents.component.scss'],
})
export class ContentsComponent implements OnInit {
  mangaName!: string;
  mangaId!: number;
  chapterId!: number;
  isLoading = true;

  contents!: any;

  constructor(
    private mangaService: MangaService,
    private route: ActivatedRoute,
    private http: HttpClient,
    private router: Router,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.mangaName = params['name'];
      this.mangaId = params['mangaId'];
      this.chapterId = params['chapterId'];
      this.http
        .get(
          `${environment.firebaseRestAPI}/manga/${this.mangaId}/chapters/${
            this.chapterId - 1
          }.json`
        )
        .subscribe((resData) => {
          //check content
          if (!resData) {
            this.router.navigate(['home']);
          }

          this.contents = resData;
          this.isLoading = false;
        });
    });

    //add History
    this.mangaService.fetchHistory().subscribe((resData) => {
      this.mangaService.writeHistory(this.mangaId, resData).subscribe();
    });
  }

  prevoiusChapter() {
    let prevoiusChapterId = +this.chapterId - 1;
    this.router.navigate([
      'contents',
      this.mangaName,
      this.mangaId,
      prevoiusChapterId,
    ]);
  }
  nextChapter() {
    let nextChapterId = +this.chapterId + 1;
    this.router.navigate([
      'contents',
      this.mangaName,
      this.mangaId,
      nextChapterId,
    ]);
  }

  onPrevious() {
    this.router.navigate(['home']);
  }

  openDialog() {
    this.mangaService.fetchMangaDetail(this.mangaId).subscribe(
      (resData) => {
        this.dialog.open(MangaDetailDialogComponent, {
          width: '600px',
          maxWidth: '90vw',
          data: { manga: resData },
        });
      },
      (errorMessage) => {
        console.log(errorMessage);
      }
    );
  }
}
