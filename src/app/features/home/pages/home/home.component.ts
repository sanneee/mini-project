import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Manga } from 'src/app/shared/models/manga.model';
import { MangaService } from 'src/app/shared/services/manga.service';

import { MangaDetailDialogComponent } from '../../../../shared/components/manga-detail-dialog/manga-detail-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { SubscriptionDialogComponent } from 'src/app/shared/components/subscription-dialog/subscription-dialog.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class HomeComponent implements OnInit {
  mangaLists!: Manga[];
  isLoading = true;

  search: string = '';

  //Category Filter
  categories!: any[];
  selectedCategory = 'All Category';

  constructor(
    private mangaService: MangaService,
    public dialog: MatDialog,
    private route: ActivatedRoute
  ) {
    this.mangaService.fetchManga().subscribe(
      (resData) => {
        this.isLoading = false;
        this.mangaLists = resData;
      },
      (errorMessage) => {
        console.log(errorMessage);
      }
    );
  }

  ngOnInit(): void {
    //
    this.route.queryParams.subscribe((params) => {
      if (params['subscription']) {
        this.openSubsciption();
      }
    });

    //get Filter
    this.getCategories();
  }

  openSubsciption() {
    this.dialog.open(SubscriptionDialogComponent, {
      width: '500px',
    });
  }

  getCategories() {
    this.mangaService.fetchCategories().subscribe(() => {
      this.categories = this.mangaService.getCategories();
      this.categories.unshift('All Category');
    });
  }

  onSelect(selectedCategory: string) {
    this.selectedCategory = selectedCategory;
  }

  openDialog(manga: Manga) {
    this.dialog.open(MangaDetailDialogComponent, {
      width: '600px',
      maxWidth: '90vw',
      data: { manga: manga },
    });
  }
}
