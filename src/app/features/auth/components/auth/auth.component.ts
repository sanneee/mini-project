import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Observable } from 'rxjs';

import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent implements OnInit {
  authForm!: FormGroup;

  isLoginMode = true;
  errorMessage!: String;
  isLoading = false;

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit(): void {
    this.authForm = new FormGroup({
      email: new FormControl(null, [
        Validators.required,
        Validators.email,
        Validators.pattern('[A-Za-z0-9._%-]+@[A-Za-z0-9._%-]+\\.[a-z]{2,3}'),
      ]),
      password: new FormControl(null, [
        Validators.required,
        Validators.minLength(8),
      ]),
    });

    if (this.isLoginMode) {
      this.authForm.removeControl('displayName');
    } else {
      this.authForm.addControl(
        'displayName',
        new FormControl(null, [Validators.required, Validators.minLength(4)])
      );
    }
  }

  onSwitchMode() {
    this.isLoading = false;
    this.isLoginMode = !this.isLoginMode;
    this.errorMessage = '';
    this.authForm.reset();

    if (this.isLoginMode) {
      this.authForm.removeControl('displayName');
    } else {
      this.authForm.addControl(
        'displayName',
        new FormControl(null, [Validators.required, Validators.minLength(4)])
      );
    }
  }

  onSubmit() {
    if (!this.authForm.valid) {
      return;
    }

    const displayName = this.authForm.value['displayName'];
    const email = this.authForm.value['email'];
    const password = this.authForm.value['password'];

    let authObs: Observable<object>;

    this.isLoading = true;
    this.authForm.disable();

    if (this.isLoginMode) {
      authObs = this.authService.login(email, password);
    } else {
      authObs = this.authService.signup(displayName, email, password);
    }

    authObs.subscribe(
      (resData) => {
        this.isLoading = false;
        this.authForm.enable();
        this.authForm.reset();
        this.router.navigate(['/home']);
      },
      (errorMessage) => {
        this.errorMessage = errorMessage;
        this.isLoading = false;
        this.authForm.enable();
      }
    );
  }
}
