import { NgModule, isDevMode } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthComponent } from './features/auth/components/auth/auth.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

//Module
import { HomeModule } from './features/home/home.module';
import { MymangaModule } from './features/mymanga/mymanga.module';
import { ContentsModule } from './features/contents/contents.module';
import { AuthModule } from './features/auth/auth.module';
import { SharedModule } from './shared/shared.module';
import { HttpInterceptorService } from './shared/services/http-interceptor.service';
import { ServiceWorkerModule } from '@angular/service-worker';

@NgModule({
  declarations: [AppComponent, AuthComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    //Module
    // AuthModule,
    // HomeModule,
    // MymangaModule,
    // ContentsModule,
    SharedModule,

    // ServiceWorkerModule.register('/ngsw-worker.js', {
    //   registrationStrategy: 'registerImmediately',
    // }),
  ],

  // ServiceWorkerModule.register('ngsw-worker.js', {
  //   enabled: !isDevMode(),
  //   // Register the ServiceWorker as soon as the application is stable
  //   // or after 30 seconds (whichever comes first).
  //   registrationStrategy: 'registerWhenStable:30000'
  // }),
  // ],
  // providers: [
  //   {
  //     provide: HTTP_INTERCEPTORS,
  //     useClass: HttpInterceptorService,
  //     multi: true,
  //   },
  // ],
  bootstrap: [AppComponent],
})
export class AppModule {}
