/** @type {import('tailwindcss').Config} */
// module.exports = {
//   content: [],
//   theme: {
//     extend: {},
//   },
//   plugins: [],
// }

module.exports = {
  content: [
    "./src/**/*.{html,ts}",
  ],
  theme: {
    extend: {aspectRatio: {
      '3/4': '3 / 4',
    },},
  },
  plugins: [
    require('@tailwindcss/line-clamp'),
  ],

}

